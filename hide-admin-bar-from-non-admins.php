<?php
/*
Plugin Name: Hide Admin Bar From Non-Administrators
Description: Hides the WordPress admin bar for all non-admin users.
Version: 1.0
Author: Ikbal Singh
Author URI:  
*/ 
 
function wp_disable_frontend_admin_bar() {
	if ( wp_show_admin_bar() ) {
		add_filter( 'show_admin_bar', '__return_false' );
	}
}
add_action( 'wp', 'wp_disable_frontend_admin_bar' );

function wp_show_admin_bar() {
	$user = wp_get_current_user();
	$wp_show_admin_topbars = apply_filters( 'wp_show_admin_bar_roles', array( 'administrator' ) );
	if ( ! array_intersect( $wp_show_admin_topbars, $user->roles ) ) {
		return true;
	} else {
		return false;
	}
}
function wp_disable_backend_admin_bar() {
	if ( wp_show_admin_bar() ) { ?>
		<style type="text/css" media="screen">html.wp-toolbar { padding-top: 0; } #wpadminbar { display: none; }</style>
		<?php
	}
}
add_action( 'admin_print_scripts-profile.php', 'wp_disable_backend_admin_bar' );
